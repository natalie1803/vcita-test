# README #
### Deployed versions: ###

*Client Service:*
[https://vcita-todo-list-client-7h2z7yarba-uc.a.run.app](https://vcita-todo-list-client-7h2z7yarba-uc.a.run.app)

*Api Service:*
[ https://vcita-todo-list-greenberg-7h2z7yarba-uc.a.run.app/api/ui/](https://vcita-todo-list-greenberg-7h2z7yarba-uc.a.run.app/api/ui)

### To Run api service on local machine ###
* Create virtual environment
* Activate virtual environment
* (venv) $ pip install --trusted-host pypi.python.org -r ./requirements.txt 
* Run app.py
* [http://localhost:8080/api/ui/](http://localhost:8080/api/ui/)
* For testing run app_test.py

### To Run Client service on local machine ###
* (venv) $ cd todo-client/
* (venv) todo-client$ npm install
* (venv) todo-client$ ng serve
* [http://localhost:4200/](http://localhost:4200/)

### To Run API service in docker ###
* $ cd vcita-test
* vcita-test$ docker build -t vcita-api .
* vcita-test$ docker run -d -p 5000:5000 vcita-api
* [http://localhost:5000/api/ui/](http://localhost:4201/api/ui/)

*default port is 8080; you may change it to 5000 in app.py*


### To Run Client service in docker ###
* $ cd vcita-test
* vcita-test$ docker build --file="client.DockerFile" -t vcita-client .
* vcita-test$ docker run -d -p 4200:4200 vcita-client
* [http://localhost:4200/](http://localhost:4200/)

*default port is 8080; you may change it to 4200 in todo-client/server.js*



