import app
import unittest

class ApiTestCase(unittest.TestCase):

    def setUp(self):
        app.app.testing = True
        self.app = app
        self.longMessage = True
        self.test_task_descr = "test task"


    def test_get_list(self):
        result = self.app.get_list()
        self.assertEqual(type(result), type(list()), msg='{0} should be a list'.format(type(result)))
    
    def test_add_task(self):
        result = self.app.add_task(self.test_task_descr)
        self.assertIsInstance(result, self.app.datastore.Entity)
        self.assertEqual(result["descr"],self.test_task_descr)
        self.assertGreater(result["id"], 0)
        
    
    def test_change_status(self):
        query = self.app.client.query(kind="Task")   
        query.add_filter("descr","=",self.test_task_descr)
        task_to_update = list(query.fetch())[-1] 
        result = self.app.change_status_task(task_to_update.key.id)
        self.assertIsInstance(result, self.app.datastore.Entity)
        self.assertEqual(result["descr"],self.test_task_descr)
        self.assertNotEqual(result["status"], task_to_update["status"])
    
    def test_remove_task(self):
        query = self.app.client.query(kind="Task")   
        query.add_filter("descr","=",self.test_task_descr)
        task_to_delete = list(query.fetch())[-1] 
        result = self.app.remove_task(task_to_delete.key.id)
        self.assertEqual(result, {})

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(ApiTestCase)
    unittest.TextTestRunner(verbosity=2).run(suite)