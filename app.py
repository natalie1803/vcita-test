"""
A sample ToDoList server.
"""
import connexion
from google.cloud import datastore
import os
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "vcita-todo-list-greenberg-e55ea232def1.json"

client = datastore.Client()


def add_headers(resp):
    """  set response headers  """
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp


def get_list():
    try:
        query = client.query(kind="Task")   
        query.order = ["descr"]
        results = list(query.fetch())   
        for task in results:
            task.update(id=task.key.id)
        return results
    except Exception as e:     
        return {"err": str(e)}


def add_task(descr):
    try:
        new_key = client.key("Task")
        task = datastore.Entity(key=new_key)
        task.update(
            {
                "descr": descr,
                "status": False
            }
        )
        client.put(task)
        task.update(id=task.key.id)
        return task
    except Exception as e:     
        return {"err": str(e)}
    

def remove_task(task_id):
    try:
        key = client.key("Task", task_id)
        client.delete(key)
        return {}
    except Exception as e:     
        return {"err": str(e)}


def change_status_task(task_id):
    try:
        key = client.key("Task", task_id)
        task = client.get(key)
        task["status"] = not task["status"]
        client.put(task)
        return task
    except Exception as e:     
        return {"err": str(e)}


app = connexion.FlaskApp(__name__, debug=True)
app.add_api('todo-list.yml', validate_responses=True)
app.app.after_request(add_headers)

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8080)
