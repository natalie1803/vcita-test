import { Inject, forwardRef, Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../environments/environment';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';


@Injectable({
  providedIn: 'root',
})

export class TasksService {

  constructor(@Inject(forwardRef(() => HttpClient)) private http: HttpClient) { }

  private handleError(error: HttpErrorResponse) {
    return throwError((error.error.status) ? `${error.error.status} - ${error.error.detail}` : `${error.message}`);
  }

  getTasksList(): Observable<any> {
    return this.http.get(`${environment.baseApiUrl}get-list`).pipe(
      catchError(this.handleError)
    );
  }

  changeStatus(taskId: number): Observable<any> {
    return this.http.post(`${environment.baseApiUrl}change-task-status/${taskId}`, null).pipe(
      catchError(this.handleError)
    );
  }

  removeTask(taskId: number): Observable<any> {
    return this.http.post(`${environment.baseApiUrl}remove-task/${taskId}`, null).pipe(
      catchError(this.handleError)
    );
  }

  addTask(taskDescr: string): Observable<any> {
    return this.http.post(`${environment.baseApiUrl}add-task/${taskDescr}`, null).pipe(
      catchError(this.handleError)
    );
  }
}