import { Component, OnInit } from '@angular/core';
import { TasksService } from './api.service';
import { Task } from './task.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'todo-client';
  data: Task[] = [];
  error: string = '';
  showForm: boolean = false;
  newTask: string = '';

  constructor(private apiService: TasksService) {

  }

  ngOnInit(): void {
    this.getTasks();
  }

  getTasks(): void {
    this.apiService.getTasksList().subscribe(
      {
        next: tasks => this.data = tasks,
        error: err => this.error = err
      }
    );
  }

  changeStatus(task: Task, target) {
    this.apiService.changeStatus(task.id).subscribe({
      next: x => task.status = !task.status,
      error: err => { this.error = err; target.checked = !target.checked; }
    }
    );
  }

  removeTask(taskId: number) {
    this.apiService.removeTask(taskId).subscribe(
      {
        next: resp => this.data.splice(this.data.findIndex(item => item.id === taskId), 1),
        error: err => this.error = err
      }
    );
  }

  addTask() {
    this.apiService.addTask(this.newTask).subscribe(
      {
        next: task => { this.data.push(task); this.newTask = ''; this.data.sort((a,b)=> (a.descr > b.descr ? 1 : -1))},
        error: err => this.error = err
      }
    );
  }

}
