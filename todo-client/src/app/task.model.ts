export class Task {
    id: number = 0;
    descr: string = '';
    status?: boolean = false;
}